



-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'clients'
-- 
-- ---

DROP TABLE IF EXISTS `clients`;
		
CREATE TABLE `clients` (
  `id` INT NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL DEFAULT 'NULL',
  `archived` TINYINT NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL,
  `freelancer_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'tasks'
-- 
-- ---

DROP TABLE IF EXISTS `tasks`;
		
CREATE TABLE `tasks` (
  `id` INT NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `value` INT NOT NULL DEFAULT 0,
  `estimated_hours` DECIMAL NOT NULL DEFAULT 0,
  `project_id` INT NULL DEFAULT NULL,
  `completed` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'projects'
-- 
-- ---

DROP TABLE IF EXISTS `projects`;
		
CREATE TABLE `projects` (
  `id` INT NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `additional_value` DECIMAL NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL,
  `milestone_id` INT NOT NULL,
  `completed` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'milestones'
-- 
-- ---

DROP TABLE IF EXISTS `milestones`;
		
CREATE TABLE `milestones` (
  `id` INT NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `completed` TINYINT NOT NULL DEFAULT 0,
  `additional_value` TINYINT NOT NULL DEFAULT 0,
  `modified` DATETIME NOT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'freelancer'
-- 
-- ---

DROP TABLE IF EXISTS `freelancer`;
		
CREATE TABLE `freelancer` (
  `id` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'time_punches'
-- 
-- ---

DROP TABLE IF EXISTS `time_punches`;
		
CREATE TABLE `time_punches` (
  `id` TINYINT NULL AUTO_INCREMENT DEFAULT NULL,
  `start_time` DATETIME NOT NULL,
  `end_time` DATETIME NULL DEFAULT NULL,
  `created` DATETIME NOT NULL,
  `modified` DATETIME NOT NULL,
  `task_id` INT NOT NULL,
  `freelancer_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `clients` ADD FOREIGN KEY (freelancer_id) REFERENCES `freelancer` (`id`);
ALTER TABLE `tasks` ADD FOREIGN KEY (project_id) REFERENCES `projects` (`id`);
ALTER TABLE `projects` ADD FOREIGN KEY (milestone_id) REFERENCES `milestones` (`id`);
ALTER TABLE `milestones` ADD FOREIGN KEY (client_id) REFERENCES `clients` (`id`);
ALTER TABLE `time_punches` ADD FOREIGN KEY (task_id) REFERENCES `tasks` (`id`);
ALTER TABLE `time_punches` ADD FOREIGN KEY (freelancer_id) REFERENCES `freelancer` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `clients` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `tasks` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `projects` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `milestones` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `freelancer` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `time_punches` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `clients` (`id`,`name`,`archived`,`created`,`modified`,`freelancer_id`) VALUES
-- ('','','','','','');
-- INSERT INTO `tasks` (`id`,`name`,`value`,`estimated_hours`,`project_id`,`completed`) VALUES
-- ('','','','','','');
-- INSERT INTO `projects` (`id`,`name`,`additional_value`,`created`,`modified`,`milestone_id`,`completed`) VALUES
-- ('','','','','','','');
-- INSERT INTO `milestones` (`id`,`name`,`completed`,`additional_value`,`modified`,`created`,`client_id`) VALUES
-- ('','','','','','','');
-- INSERT INTO `freelancer` (`id`,`name`) VALUES
-- ('','');
-- INSERT INTO `time_punches` (`id`,`start_time`,`end_time`,`created`,`modified`,`task_id`,`freelancer_id`) VALUES
-- ('','','','','','','');

