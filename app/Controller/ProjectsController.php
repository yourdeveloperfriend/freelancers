<?php


App::uses('Controller', 'Controller');



class ProjectsController extends AppController {
	public $name = 'Projects';

	public function index() {
		$clients = $this->Client->find('all');
		$results = array();
		foreach($clients as $client) {
			array_push($results, $client['Client']);
		}
		$this->renderJson($results);
	}

	public function tasks() {
		$tasks = $this->Project->Task->findAllByProjectId($this->request->params['id']);
		$results = array();
		foreach($tasks as $tasks) {
			array_push($results, $tasks['Task']);
		}
		$this->renderJson($results);
	}

}
