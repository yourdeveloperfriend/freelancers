<?php


App::uses('Controller', 'Controller');



class TasksController extends AppController {
	public $name = 'Tasks';

	public function index() {
		$clients = $this->Client->find('all');
		$results = array();
		foreach($clients as $client) {
			array_push($results, $client['Client']);
		}
		$this->renderJson($results);
	}

	public function projects() {
		$this->renderJson($this->Client->Milestone->Project->findAllByClientId($this->request->params['id']));
	}

}
