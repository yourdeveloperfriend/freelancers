<?php


App::uses('Controller', 'Controller');



class TimePunchesController extends AppController {
	public $name = 'TimePunches';

	public function index() {
		$this->renderJson($this->TimePunch->find('all'));
	}

	public function start() {
		$task_id = $this->request->data['task_id'];
		$now = date("Y-m-d H:i:s");
		$this->TimePunch->start($this->user_id, $task_id, $now);
		$this->renderJson(array('success' => true, 'time' => $now));
	}
	public function stop() {
		$now = date("Y-m-d H:i:s");
		$this->TimePunch->stop($this->user_id, $now);
		$this->renderJson(array('success' => true, 'time' => $now));
	}
}
