<?php

class Project extends AppModel {
	public $belongsTo = array('Milestone');
	public $hasMany = array('Task');

	public function findAllByClientId($client_id) {
		$milestones = $this->Milestone->findAllByClientId($client_id, 'Milestone.id');
		$results = array();
		foreach($milestones as $milestone) {
			$results = array_merge($results, $milestone['Project']);
		}

		return $results;
	}

}
