<?php

class TimePunch extends AppModel {
	public $belongsTo = array('Freelancer', 'Task');

	public function start($user_id, $task_id, $time) {
		$this->stop($user_id, $time);
		$this->create();
		$this->save(array(
			'start_time' => $time,
			'task_id' => $task_id,
			'freelancer_id' => $user_id
		));
		return $this->id;
	}

	public function stop($user_id, $time) {
		$punch = $this->find('first', array('conditions' => array('end_time' => null, 'freelancer_id' => $user_id)));
		if($punch) {
			$punch['TimePunch']['end_time'] = $time;
			$this->save($punch);
		}
		return $punch;
	}
}
