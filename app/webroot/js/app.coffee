$(->
	Application = Backbone.Router.extend
		routes:
			'': 'index'
		index: ->
			@view = new IndexView()
			@render()
	
		render: ->
			$("#body").append @view.$el
			@view.render()

	window.App = new Application()
	

	Backbone.history.start pushState: true
)