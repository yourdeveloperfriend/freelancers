window.BaseView = Backbone.View.extend
	tagName: 'div'
	initialize: ->
		@template or= @className
		@template_function = @getTemplate @template

	getTemplate: (template)->
		new EJS
			url: "/js/templates/#{template}.ejs"

	render: ->
		@onBeforeRender?()
		@renderHtml()
		@renderChildren()
		@onAfterRender?()

	renderHtml: ->
		@$el.html @template_function.render(@)

	renderChildren: ->
		_t = @
		@$('[data-child]').each ->
			child = _t[$(this).data('child')]($(this))
			$(this).html child.$el
			child.render()

