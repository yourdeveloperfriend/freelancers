class window.IndexView extends BaseView
	className: 'index'

	initialize: (options)->
		super

		@selected = new Backbone.Model()
		@lists =
			clients: new Backbone.Collection [],
				url: '/api/clients'
			projects: new Backbone.Collection []
			tasks: new Backbone.Collection []
		@lists.clients.fetch()
		@lists.clients.on 'sync', @render, @
		@selected.on 'change:clients', @changeSelected, @
		@selected.on 'change:projects', @changeSelected, @
		@selected.on 'change:tasks', @render, @
		@selected.on 'change:time', @render, @
		@views = []
		@selected.set
			clients: 1
			projects: 1
			tasks: 1

	events:
		'click [data-action="start-clock"]': 'startClock'
		'click [data-action="stop-clock"]': 'stopClock'

	createTimer: ->
		@timer = new TimerView
			timer: @selected

	startClock: ->
		task_id = @selected.get 'tasks'
		if task_id
			@selected.set 'time', new Date()
			$.post('/api/time_punches/start', data: task_id: task_id
			).done (result)=>
				if result.success
					@selected.set 'time', new Date result.time

	stopClock: ->
		$.post('/api/time_punches/stop')
		@selected.set 'time', null

	changeSelected: (model)->
		if model.changed.clients?
			@checkChanged(model, 'clients', 'projects', 'tasks')
		if model.changed.projects?
			@checkChanged(model, 'projects', 'tasks')

	checkChanged: (model, changed, child, subchild)->
		url = if model.changed[changed] isnt '' then "/api/#{changed}/#{model.changed[changed]}/#{child}"
		unless @lists[child].url is url
			@lists[child].reset()
			@lists[child].url = url
			if url
				@lists[child].fetch()
			else if subchild
				@lists[subchild].reset()

	createList: ($el)->
		type = $el.data('list')
		collection = @lists[type]
		@views[type] = new SelectView
			selected: @selected
			variable: type
			collection: @lists[type]
