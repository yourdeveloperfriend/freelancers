class window.SelectView extends BaseView
	className: 'select'

	initialize: (options)->
		super

		@collection = options.collection
		@selected = options.selected
		@variable = options.variable
		@collection.on 'sync add remove reset', @onCollectionChange, @

	events:
		'change select': 'changeSelect'

	onCollectionChange: ->
		selected = @collection.get @selected.get @variable
		@selected.set @variable, '' unless selected
		@render()

	changeSelect: (e)->
		@selected.set @variable, $(e.target).val()

