class window.TimerView extends BaseView
	className: 'timer'

	initialize: (options)->
		super
		@timer = options.timer
		setInterval @time.bind(@), 10
		@timer.on 'change:time', @time, @
		@time false

	time: (render = true)->
		if @timer.get('time')
			now = new Date().getTime()
			time = @timer.get('time').getTime()
			distance = now - time
			@milli = @leftPad(Math.floor(distance % 1000 / 10))
			@second = @leftPad(Math.floor(distance % 60000 / 1000))
			@minute = @leftPad(Math.floor(distance % 3600000 / 60000))
			@hour = @leftPad(Math.floor(distance / 3600000))
			@render() if render

	leftPad: (number)->
		pad = "" + number
		if pad.length is 1
			"0" + pad
		else
			pad
		
